"use strict";

// EJERCICIO BANCO - 2ª PARTE:

/* En la clase Banco he creado dos métodos:

- uno para bloquear la cuenta (es un método estático);
- otro para desbloquearla (método no estático).

*/

class Banco {
  constructor(nombre, direccion) {
    this.nombre = nombre;
    this.direccion = direccion;
    this.clientes = [];
  }

  /* Si el id del titular de la cuenta no es igual al id de la cuenta, el banco
  bloquea la cuenta. Hago un método estático que luego "llamaré" desde la clase
  Titular, para los ingresos y retiradas de dinero. */

  static bloquearCuenta(titular, cuenta) {
    if (titular.id !== cuenta.id) {
      cuenta.access = false;
      return "La cuenta ha sido bloqueada";
    }
  }

  /* Para desbloquear la cuenta, hay que solicitarlo al Banco.  */

  desbloquearCuenta(titular, cuenta) {
    if (titular.id === cuenta.id && cuenta.access === false) {
      cuenta.access = true;
      return "La cuenta ha sido desbloqueada";
    }
  }
}

class CuentaBancaria {
  constructor(id) {
    this.balance = 0;
    this.id = id;
    this.access = true;
  }
}

class Titular {
  constructor(nombre, genero, monedero, id) {
    this.nombre = nombre;
    this.genero = genero;
    this.monedero = monedero;
    this.id = id;
  }

  abrirCuentaBancaria(banco) {
    banco.clientes.push({ nombre: this.nombre, id: this.id });
    return new CuentaBancaria(this.id);
  }

  ingresarDinero(cantidad, cuenta) {
    /* Primero comprobamos si el id del titular que quiere hacer la operación
     coincide con el id de la cuenta en la que quiere operar: 

      - si los id's coinciden, luego se comprueba si hay dinero suficiente en el monedero;
      
      - si los id's no coinciden, el banco bloquea la cuenta automáticamente. Aquí "llamo"
        al método estático de la clase Banco. */

    if (this.id === cuenta.id) {
      if (this.monedero < cantidad) {
        return `No tiene suficiente dinero en el monedero para hacer este ingreso.`;
      } else {
        this.monedero -= cantidad;
        cuenta.balance += cantidad;
        return `Ha ingresado ${cantidad} euros en su cuenta. El saldo actual de su cuenta es de ${cuenta.balance} euros.`;
      }
    } else {
      return Banco.bloquearCuenta(this.id, cuenta);
    }
  }

  retirarDinero(cantidad, cuenta) {
    /* Aquí hacemos lo mismo que en el método anterior: */
    if (this.id === cuenta.id) {
      if (cuenta.balance < cantidad) {
        return `No tiene suficiente saldo en cuenta para hacer esta retirada.`;
      } else {
        cuenta.balance -= cantidad;
        this.monedero += cantidad;
        return `Ha retirado ${cantidad} euros de su cuenta. El saldo actual de su cuenta es de ${cuenta.balance} euros. El ingreso de ${cantidad} euros ha sido realizado en su monedero.`;
      }
    } else {
      return Banco.bloquearCuenta(this.id, cuenta);
    }
  }

  mostrarSaldo(cuenta) {
    return `El saldo actual de su cuenta es de ${cuenta.balance} euros.`;
  }
}

/* 1. Creo dos instancias de la class Titular: */
const manolo = new Titular("Manolo", "hombre", 1200, "5879");
const federico = new Titular("Federico", "hombre", 2100, "3647");
console.log(manolo, federico);

/* 2. Creo instancia de la class Banco: */
const bancoUno = new Banco("BancoSantander", "Linares Rivas, 6. A Coruña");
console.log(bancoUno);

/* 3. Abro una cuenta bancaria para cada titular :*/

const cuentaManolo = manolo.abrirCuentaBancaria(bancoUno);
const cuentaFederico = federico.abrirCuentaBancaria(bancoUno);
console.log(cuentaManolo, cuentaFederico);

/*4. Manolo intenta ingresar dinero en la cuenta de Federico. 
     La cuenta de Federico queda bloqueada.
     Federico pide al banco que le desbloquee la cuenta */

console.log(manolo.ingresarDinero(200, cuentaFederico));
console.log(cuentaFederico);
console.log(bancoUno.desbloquearCuenta(federico, cuentaFederico));
console.log(cuentaFederico);

/*5. Manolo intenta retirar dinero en la cuenta de Federico.
     La cuenta de Federico queda bloqueada.
     Federico pide al banco que le desbloquee la cuenta */

console.log(manolo.retirarDinero(200, cuentaFederico));
console.log(cuentaFederico);
console.log(bancoUno.desbloquearCuenta(federico, cuentaFederico));
console.log(cuentaFederico);


/**
 * ##########################################
 * #### S E A T I N G    S T U D E N T S ####
 * ##########################################
 */

/**
 * En un aula encontramos un numero indeterminado de
 * pupitres organizados en dos columnas. Algunos
 * pupitres estÃ¡n ocupados mientras otros estÃ¡n vacÃ­os.
 *
 * El array "bussyDesks" sigue el siguiente formato:
 * `[K, r1, r2, r3, ...] ` donde `K` es el total de
 * pupitres que hay en el aula. El resto de valores
 * se corresponden a los pupitres que ya estÃ¡n
 * ocupados.
 *
 * Debes averiguar la cantidad de formas en que 2
 * estudiantes pueden sentarse uno al lado del otro.
 * Esto significa que 1 estudiante esta a la izquierda
 * y 1 estudiante a la derecha, o que 1 estudiante esta
 * directamente arriba o debajo de otro estudiante.
 *
 * Ejemplo: `[12, 2, 6, 7, 11]`
 *
 * Basandose en esta disposicion de escritorios ocupados,
 * hay un total de 6 formas de acomodar a 2 nuevos
 * estudiantes uno al lado del otro. Las combinaciones
 * son:
 *
 * `[1, 3], [3, 4], [3, 5], [8, 10], [9, 10], [10, 12]`
 *
 * Entonces, para el array del ejemplo, el programa
 * debera devolver 6.
 *
 * `K` variara de 2 a 24 y siempre sera un numero par.
 * Despues de `K`, el numero de escritorios ocupados en
 * la matriz puede variar de `0` a `K`.
 */

const bussyDesks = [12, 2, 6, 7, 11];
// elimino el primer elemento y lo devuelo
const numberOfDesks = bussyDesks.shift();
console.log(bussyDesks);
console.log(numberOfDesks);

let result = 0;

/* Hay 12 pupitres colocados en 2 filas: una fila con los impares y otra con los pares:

    1     2
    3     4
    5     6
    7     8
    9     10
    11    12

 Hacemos una función con un bucle "for" que va a recorrer los pupitres con los números 
 del 1 al 11. Los números impares se comparan con el de la derecha y con el de abajo; los
 numeros pares se comparan con el de abajo (por eso el 12 no hace falta incluirlo en el
 blucle for) */

function findFreePositions() {
  for (let i = 1; i < numberOfDesks; i++) {
    /* primero compruebo si el número "i" es un pupitre libre. Si el número "i" existe en 
    el array de bussyDesks, su indexOf será su posición en el array (por ejemplo, el 
    indexOf(7) será 2). Si el número "i" no existe en el array de bussyDesks, su indexOf 
    será -1. Cada vez que haya 2 pupitres juntos libres, el resultado se incrementa en 1. */

    if (bussyDesks.indexOf(i) === -1) {
      // si el libre es impar:
      if (i % 2 !== 0) {
        const right = i + 1;
        const down = i + 2;
        if (bussyDesks.indexOf(right) === -1) {
          result++;
        }
        if (bussyDesks.indexOf(down) === -1) {
          result++;
        } // si el libre es par:
      } else {
        const down = i + 2;
        if (bussyDesks.indexOf(down) === -1) {
          result++;
        }
      }
    }
  }
  return result;
}

console.log(findFreePositions(result));

